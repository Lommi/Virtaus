/*  Copyright 2017 Juho Lommi
 *
 *  This file is part of Virtaus.
 *
 *  Virtaus is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Virtaus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Virtaus. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <time.h>
#include "deps/SDL2/SDL.h"
#include "deps/SDL2/SDL_image.h"
#include "deps/SDL2/SDL_ttf.h"
#include "deps/SDL2/SDL_mixer.h"

#define SCREEN_W 640
#define SCREEN_H 640
#define SPRITE_W 16
#define ENEMY_ALLOCATIONS 20
#define NUM_RECT_POINTS 5
#define NUM_PLAYER_PARTS 20
#define BUFSIZE 32
#define STAR_TIMER_LIMIT 15
#define STAR_SPEED 5
#define PLAYER_EF_SPEED 4
#define ENEMY_SPAWN_TIMER_LIMIT 10
#define STAR_TIMER_LIMIT 15
#define SCORE_LIMIT 4294967295
#define ENEMYCOLORS 6
#define FPS_CAP 60
#define MAX_VOLUME 50

#define PI 3.1415926535897
#define RADIAN PI / 180

typedef struct Gameobj Gameobj;

struct Gameobj
{
SDL_Texture *tex;
SDL_Rect    rec;
int         speed;
float       x, y, dir;
};

enum GameState
{
GS_EXIT = 0,
GS_MENU,
GS_GAME,
GS_PAUSE,
GS_DEAD
};

enum Playerdir
{
DIR_RIGHT = 0,
DIR_DOWN,
DIR_LEFT,
DIR_UP
};

SDL_Window   *window;
SDL_Renderer *renderer;

static unsigned int score;
static unsigned int highscore, highscore2;
static int      fps;
static int      gamestate;
static int      num_textures;
static int      text_w;
static int      tmp;
static int      num_enemies;
static int      max_num_enemies;
static int      difficulty;
static int      spawn_enemies;
static int      enemy_spawn_timer;
static int      star_ef_timer;
static int      star_ef_active;
static int      newhighscore;
static int      buflen;
static int      turncounter;
static int      muted;
static int      enemycolor;
static int      counted_frames;
static uint16_t screen_area;
static uint32_t tick_start, tick_frame;
static uint32_t cap_start, cap_frame;
static float    avg_fps, time_step;
static char     score_buf[BUFSIZE];
static char     highscore_buf[BUFSIZE];
static char     tmp_buf[BUFSIZE];
static char     fps_buf[BUFSIZE];
static int      print_fps;
const int       TICK_PER_FRAME = 1000 / FPS_CAP;

static Mix_Chunk *snd_turn1 = 0, *snd_turn2 = 0;
static Mix_Chunk *snd_star = 0;
static Mix_Chunk *snd_explosion = 0;
static Mix_Chunk *snd_select = 0;
static SDL_Surface *icon = 0;
static SDL_Texture *textures[4];
static SDL_Texture *text_tex;
static TTF_Font *main_font     = 0;
static SDL_Rect clip16x16      = {0, 0, 16, 16};
static SDL_Rect cliplogo       = {0, 0, 428, 76};
static SDL_Rect logopos        = {SCREEN_W / 2 - 428 / 2, 64, 428, 76};
static SDL_Rect menu_black_box = {SCREEN_W / 2 - 116, SCREEN_H / 2 - 116, 232, 356}; //button
static SDL_Color c_white       = {0xFF, 0xFF, 0xFF, 0xFF};
static SDL_Color c_black       = {0x00, 0x00, 0x00, 0xFF};
static SDL_Color c_red         = {0xFF, 0x00, 0x00, 0xFF};
static SDL_Color c_green       = {0x00, 0xFF, 0x00, 0xFF};
static SDL_Color c_blue        = {0x00, 0x00, 0xFF, 0xFF};
static SDL_Color c_yellow      = {0xFF, 0xFF, 0x00, 0xFF};
static SDL_Color c_purple      = {0xFF, 0x00, 0xFF, 0xFF};
static SDL_Color c_aqua        = {0x00, 0xFF, 0xFF, 0xFF};
static SDL_Point startbtn[NUM_RECT_POINTS] = {
{SCREEN_W / 2 - 100 , SCREEN_H / 2 - 100},
{SCREEN_W / 2 + 100 , SCREEN_H / 2 - 100},
{SCREEN_W / 2 + 100 , SCREEN_H / 2 + 100},
{SCREEN_W / 2 - 100 , SCREEN_H / 2 + 100},
{SCREEN_W / 2 - 100 , SCREEN_H / 2 - 100}};
static float star_ef[NUM_PLAYER_PARTS][2]       = {0};
static float star_ef_angles[NUM_PLAYER_PARTS]   = {0};
static float player_ef[NUM_PLAYER_PARTS][2]     = {0};
static float player_ef_angles[NUM_PLAYER_PARTS] = {0};

static Gameobj player;
static Gameobj star;
static Gameobj enemy[ENEMY_ALLOCATIONS];
static FILE *f;

int  init();
int  shutdown();
void render();
void gameUpdate();
void input();
void restart(int gs);
SDL_Texture *loadTexture(const char *fp);
TTF_Font *loadFont(const char *fp, int size);
void renderText(SDL_Renderer *renderer, int x, int y,
const char *text, TTF_Font *font, SDL_Color color);
inline void fpsStart();
inline void fpsEnd();
inline void playTurnSound();
inline void spawnEnemies();
inline void changeEnemyColor();

int main(int argc, char **argv)
{
init();
while(gamestate)
{
    fpsStart();
    input();
    gameUpdate();
    render();
    fpsEnd();
}
shutdown();

return 0;
}

int init()
{
    window       = 0;
    renderer     = 0;
    num_textures = 0;

    gamestate = GS_MENU;

    srand(time(0));

    if (SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) < 0){ printf("SDL_Init failure, SDL_Error: %s\n", SDL_GetError()); exit(0); }

    window = SDL_CreateWindow("",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        SCREEN_W, SCREEN_H, SDL_WINDOW_SHOWN);

    if (!window){ printf("SDL_CreateWindow failure, SDL Error: %s\n", SDL_GetError()); exit(0); }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (!renderer){ printf("SDL_CreateRenderer failure, SDL Error: %s\n", SDL_GetError()); exit(0); }

    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) { printf("SDL_Image init failure, SDL_Image Error: %s\n", IMG_GetError()); exit(0); }

    if (TTF_Init() == -1){ printf("SDL_ttf init failure, SDL_ttf Error: %s\n", TTF_GetError()); }

	if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		printf( "Warning: Linear texture filtering not enabled!" );

    if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 1024 ) == -1 )
        printf("SDL_Mixer init failed\n");

    icon = IMG_Load("../assets/star_icon.ico");
    SDL_SetWindowIcon(window, icon);
    SDL_FreeSurface(icon);

    Mix_AllocateChannels(4);
    Mix_Volume(0, MAX_VOLUME);
    Mix_Volume(1, MAX_VOLUME);
    Mix_Volume(2, MAX_VOLUME);
    Mix_Volume(3, MAX_VOLUME);

    SDL_ShowCursor(SDL_DISABLE);

    screen_area = (SCREEN_W - 128) / SPRITE_W;

    player.rec.x = SCREEN_W / 2 - 128;
    player.rec.y = SCREEN_H / 2 + 50;
    player.rec.w = SPRITE_W;
    player.rec.h = SPRITE_W;
    player.dir   = DIR_RIGHT;
    player.speed = 4;

    star.rec.x = SCREEN_W / 2;
    star.rec.y = SCREEN_H / 2 - 50;
    star.rec.w = SPRITE_W;
    star.rec.h = SPRITE_W;

    textures[0] = loadTexture("../assets/player.png");
    textures[1] = loadTexture("../assets/star.png");
    textures[2] = loadTexture("../assets/enemy.png");
    textures[3] = loadTexture("../assets/logo.png");

    main_font  = loadFont("../assets/OrbitronMedium.TTF", 24);

    snd_turn1 = Mix_LoadWAV("../assets/turn1.wav");
    snd_turn2 = Mix_LoadWAV("../assets/turn2.wav");
    snd_star = Mix_LoadWAV("../assets/star.wav");
    snd_explosion = Mix_LoadWAV("../assets/explosion.wav");
    snd_select = Mix_LoadWAV("../assets/select.wav");
    if (!snd_turn1) puts("turn1.wav failed to load\n");
    if (!snd_turn2) puts("turn2.wav failed to load\n");
    if (!snd_star) puts("star.wav failed to load\n");
    if (!snd_explosion) puts("explosion.wav failed to load\n");
    if (!snd_select) puts("select.wav failed to load\n");

    player.tex = textures[0];
    star.tex   = textures[1];
    for (int i = 0; i < ENEMY_ALLOCATIONS; ++i)
    {
        enemy[i].tex = textures[2];
        enemy[i].speed = 0;
        enemy[i].x     = -10;
        enemy[i].y     = -10;
        enemy[i].rec.x = -10;
        enemy[i].rec.y = -10;
        enemy[i].rec.w = SPRITE_W;
        enemy[i].rec.h = SPRITE_W;
    }

    turncounter = 0;

    score = 0;
    sprintf(score_buf, "%d", score);

    star_ef_timer     = 0;
    star_ef_active    = 0;
    max_num_enemies   = ENEMY_ALLOCATIONS;
    num_enemies       = 0;
    spawn_enemies     = 1;
    enemy_spawn_timer = 0;
    enemycolor        = 4;

    muted             = 0;
    difficulty        = ENEMY_ALLOCATIONS;
    highscore         = 0;
    highscore2        = 0;
    newhighscore      = 0;

    counted_frames    = 0;
    tick_start        = 0;
    avg_fps           = 0;
    print_fps         = 0;

    f = 0;
    fopen("save.txt", "a");
    f = fopen("save.txt", "r");
    char line[256];
    if (!f) printf("failed to open save.txt\n");
    else
    {
        fscanf(f, "hs: %d\n", &highscore);
        fscanf(f, "hs2: %d\n", &highscore2);
        fscanf(f, "d: %d\n", &difficulty);
        fscanf(f, "ec: %d\n", &enemycolor);
        fscanf(f, "m: %d", &muted);
    }
    fclose(f);

    if (!difficulty) sprintf(highscore_buf, "HIGHSCORE: %d", highscore);
    else sprintf(highscore_buf, "HIGHSCORE: %d", highscore2);

    tick_start = SDL_GetTicks();

    if (muted)
    {
        Mix_Volume(0, 0);
        Mix_Volume(1, 0);
        Mix_Volume(2, 0);
        Mix_Volume(3, 0);
    }

    changeEnemyColor();

    return 0;
}

int shutdown()
{
    if (score > highscore && difficulty == 0)
        highscore = score;
    else if (score > highscore2 && difficulty == 1)
        highscore2 = score;
    f = fopen("save.txt", "w");
    if (!f) printf("failed to open save.txt\n");
    else
    {
         sprintf(highscore_buf, "hs: %d\n", highscore);
         buflen = strlen(highscore_buf);
         fwrite(highscore_buf, 1, buflen, f);
         sprintf(highscore_buf, "hs2: %d\n", highscore2);
         buflen = strlen(highscore_buf);
         fwrite(highscore_buf, 1, buflen, f);
         sprintf(tmp_buf, "d: %d\n", difficulty);
         buflen = strlen(tmp_buf);
         fwrite(tmp_buf, 1, buflen, f);
         sprintf(tmp_buf, "ec: %d\n", enemycolor);
         buflen = strlen(tmp_buf);
         fwrite(tmp_buf, 1, buflen, f);
         sprintf(tmp_buf, "m: %d", muted);
         buflen = strlen(tmp_buf);
         fwrite(tmp_buf, 1, buflen, f);
    }
    fclose(f);

    Mix_FreeChunk(snd_turn1);
    Mix_FreeChunk(snd_turn2);
    Mix_FreeChunk(snd_star);
    Mix_FreeChunk(snd_explosion);
    Mix_FreeChunk(snd_select);
    Mix_CloseAudio();

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window   = 0;
    renderer = 0;
    IMG_Quit();
    SDL_Quit();

    return 0;
}

void render()
{
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

    switch(gamestate)
    {
        case GS_MENU:
        {
            for (int i = 0; i < num_enemies; ++i)
                SDL_RenderCopyEx(renderer, enemy[i].tex, &clip16x16, &enemy[i].rec, enemy[i].dir, 0, 0);
            SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
            SDL_RenderFillRect(renderer, &menu_black_box);
            SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
            SDL_RenderDrawLines(renderer, startbtn, NUM_RECT_POINTS);
            renderText(renderer, SCREEN_W / 2 - 48, SCREEN_H / 2 - 24, "PRESS", main_font, c_white);
            renderText(renderer, SCREEN_W / 2 - 48, SCREEN_H / 2 + 16, "ENTER", main_font, c_white);
            renderText(renderer, SCREEN_W / 2 - 76, SCREEN_H / 2 + 124, "DIFFICULTY", main_font, c_white);
            renderText(renderer, SCREEN_W / 2 - 76, SCREEN_H / 2 + 156, "<                    >", main_font, c_white);
            if (!difficulty) renderText(renderer, SCREEN_W / 2 - 56, SCREEN_H / 2 + 156, "NORMAL", main_font, c_white);
            else renderText(renderer, SCREEN_W / 2 - 32, SCREEN_H / 2 + 156, "HARD", main_font, c_white);
            renderText(renderer, SCREEN_W / 2 - 104, SCREEN_H / 2 + 214, highscore_buf, main_font, c_white);
            if (!muted) renderText(renderer, SCREEN_W - 168, SCREEN_H - 24, "M=MUTE ON", main_font, c_white);
            else renderText(renderer, SCREEN_W - 184, SCREEN_H - 24, "M=MUTE OFF", main_font, c_white);
            renderText(renderer, 12, SCREEN_H - 24, "R=RESET SCORE", main_font, c_white);
            SDL_RenderCopy(renderer, textures[3], &cliplogo, &logopos);
        }break;
        case GS_PAUSE: renderText(renderer, SCREEN_W / 2 - 40, SCREEN_H / 2, "PAUSED", main_font, c_white);
        case GS_DEAD:
        {
            if (gamestate != GS_PAUSE)
            {
                for (int i = 0; i < NUM_PLAYER_PARTS; ++i)
                    SDL_RenderDrawPoint(renderer, (int)player_ef[i][0], (int)player_ef[i][1]);
            }
        }
        case GS_GAME:
        {
            SDL_RenderCopyEx(renderer, star.tex, &clip16x16, &star.rec, 0, 0, 0);

            for (int i = 0; i < num_enemies; ++i)
                SDL_RenderCopyEx(renderer, enemy[i].tex, &clip16x16, &enemy[i].rec, enemy[i].dir, 0, 0);

            if (star_ef_active)
            {
                SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0x00, 0xFF);
                for (int i = 0; i < NUM_PLAYER_PARTS; ++i)
                    SDL_RenderDrawPoint(renderer, (int)star_ef[i][0], (int)star_ef[i][1]);
            }

            renderText(renderer, 8, 8, score_buf, main_font, c_white);
            renderText(renderer, 8, 32, highscore_buf, main_font, c_white);

            if (gamestate != GS_DEAD)
                SDL_RenderCopyEx(renderer, player.tex, &clip16x16, &player.rec, 90 * player.dir, 0, 0);
            else
            {
                renderText(renderer, SCREEN_W / 2 - 80, SCREEN_H / 2, "GAME OVER", main_font, c_white);
                if (newhighscore)
                    renderText(renderer, SCREEN_W / 2 - 120, SCREEN_H / 2 + 32, "NEW HIGH SCORE!", main_font, c_white);
            }

        }break;
    }
    if (print_fps)
        renderText(renderer, SCREEN_W / 2, 16, fps_buf, main_font, c_white);
    SDL_RenderPresent(renderer);
}

void gameUpdate()
{
    switch (gamestate)
    {
        case GS_DEAD:
        {
            for (int i = 0; i < NUM_PLAYER_PARTS; ++i)
            {
                if (player_ef[i][0] > SCREEN_W ||
                    player_ef[i][0] < 0 ||
                    player_ef[i][1] > SCREEN_H ||
                    player_ef[i][1] < 0)
                {
                    player_ef[i][0] = -10;
                    player_ef[i][1] = -10;
                }
                else
                {
                    player_ef[i][0] += cos(player_ef_angles[i] * RADIAN) * PLAYER_EF_SPEED;
                    player_ef[i][1] += sin(player_ef_angles[i] * RADIAN) * PLAYER_EF_SPEED;
                }
            }
        }
        case GS_GAME:
        {
            if (star_ef_active)
            {
                ++star_ef_timer; if (star_ef_timer > STAR_TIMER_LIMIT) star_ef_active = 0;
                for (int i = 0; i < NUM_PLAYER_PARTS; ++i)
                {
                    star_ef[i][0] += cos(star_ef_angles[i] * RADIAN) * STAR_SPEED;
                    star_ef[i][1] += sin(star_ef_angles[i] * RADIAN) * STAR_SPEED;
                }
            }

            switch((int)player.dir)
            {
                case DIR_UP: player.rec.y    -= player.speed; break;
                case DIR_RIGHT: player.rec.x += player.speed; break;
                case DIR_DOWN: player.rec.y  += player.speed; break;
                case DIR_LEFT: player.rec.x  -= player.speed; break;
            }

            if (player.rec.x + SPRITE_W > star.rec.x &&
                player.rec.x < star.rec.x + SPRITE_W &&
                player.rec.y + SPRITE_W > star.rec.y &&
                player.rec.y < star.rec.y + SPRITE_W &&
                gamestate == GS_GAME)
            {
                star_ef_active = 1;
                star_ef_timer = 0;
                for (int i = 0; i < NUM_PLAYER_PARTS; ++i)
                {
                    star_ef[i][0] = star.rec.x;
                    star_ef[i][1] = star.rec.y;
                    star_ef_angles[i] = 30 * i;
                }

                star.rec.x = 64 + SPRITE_W * (rand() % screen_area);
                star.rec.y = 64 + SPRITE_W * (rand() % screen_area);
                if (score < SCORE_LIMIT) ++score;
                Mix_PlayChannel( -1, snd_star, 0 );
                sprintf(score_buf, "%d", score);

                if (!spawn_enemies && difficulty == 1) spawn_enemies = 1;
                else if (difficulty == 0 && num_enemies < ENEMY_ALLOCATIONS)
                {
                    enemy[num_enemies].speed = 4;
                    tmp = rand() % 4;
                    switch(tmp)
                    {
                        case 0:
                        {
                            enemy[num_enemies].x = rand() % SCREEN_W;
                            enemy[num_enemies].y = 16;
                            enemy[num_enemies].dir = 5 + rand() % 170;
                        }break;
                        case 1:
                        {
                            enemy[num_enemies].x = 16;
                            enemy[num_enemies].y = rand() % SCREEN_H;
                            enemy[num_enemies].dir = -95 + rand() % 170;
                        }break;
                        case 2:
                        {
                            enemy[num_enemies].x = rand() % SCREEN_W;
                            enemy[num_enemies].y = SCREEN_H - 16;
                            enemy[num_enemies].dir = 185 + rand() % 170;
                        }break;
                        case 3:
                        {
                            enemy[num_enemies].x = SCREEN_W - 16;
                            enemy[num_enemies].y = rand() % SCREEN_H;
                            enemy[num_enemies].dir = 95 + rand() % 170;
                        }break;
                    }
                    ++num_enemies;
                }
            }

            if (player.rec.x < 0) player.rec.x = SCREEN_W - SPRITE_W;
            if (player.rec.x > SCREEN_W - SPRITE_W) player.rec.x = 0;
            if (player.rec.y < 0) player.rec.y = SCREEN_H - SPRITE_W;
            if (player.rec.y > SCREEN_H - SPRITE_W) player.rec.y = 0;
        }
        case GS_MENU:
        {
            spawnEnemies();

            for (int i = 0; i < num_enemies; ++i)
            {
                enemy[i].x += cos(enemy[i].dir * RADIAN) * enemy[i].speed;
                enemy[i].y += sin(enemy[i].dir * RADIAN) * enemy[i].speed;
                enemy[i].rec.x = (int)enemy[i].x;
                enemy[i].rec.y = (int)enemy[i].y;

                if (player.rec.x + SPRITE_W > enemy[i].rec.x &&
                    player.rec.x < enemy[i].rec.x + SPRITE_W &&
                    player.rec.y + SPRITE_W > enemy[i].rec.y &&
                    player.rec.y < enemy[i].rec.y + SPRITE_W &&
                    gamestate == GS_GAME)
                {
                    gamestate = GS_DEAD;
                    Mix_PlayChannel( -1, snd_explosion, 0 );
                    if (score > highscore && difficulty == 0)
                    {
                        highscore = score;
                        newhighscore = 1;
                        sprintf(highscore_buf, "%d", highscore);
                    }
                    else if (score > highscore2 && difficulty == 1)
                    {
                        highscore2 = score;
                        newhighscore = 1;
                        sprintf(highscore_buf, "%d", highscore2);
                    }
                    for (int i = 0; i < NUM_PLAYER_PARTS; ++i)
                    {
                        player_ef[i][0] = player.rec.x + SPRITE_W / 2;
                        player_ef[i][1] = player.rec.y + SPRITE_W / 2;
                        player_ef_angles[i] = rand() % 360;
                    }
                }
                if (enemy[i].x < 0) enemy[i].x = SCREEN_W - SPRITE_W;
                if (enemy[i].x > SCREEN_W - SPRITE_W) enemy[i].x = 0;
                if (enemy[i].y < 0) enemy[i].y = SCREEN_H - SPRITE_W;
                if (enemy[i].y > SCREEN_H - SPRITE_W) enemy[i].y = 0;
            }
        }break;
    }
}

void input()
{
    SDL_Event e;

    while(SDL_PollEvent(&e) != 0)
    {
        switch (e.type)
        {
            case (SDL_QUIT): gamestate = GS_EXIT; break;
            case (SDL_KEYDOWN):
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                    {
                        if (gamestate != GS_MENU) restart(GS_MENU);
                        else gamestate = GS_EXIT;
                    } break;
                    case SDLK_w:
                    case SDLK_UP:
                    {
                        if (gamestate == GS_GAME && player.dir != DIR_UP)
                        {
                            player.dir = DIR_UP;
                            playTurnSound();
                        }
                        else if (gamestate == GS_MENU)
                        {
                            ++enemycolor; if (enemycolor > ENEMYCOLORS) enemycolor = 0;
                            changeEnemyColor();
                        }
                    } break;
                    case SDLK_d:
                    case SDLK_RIGHT:
                    {
                        if (gamestate == GS_GAME && player.dir != DIR_RIGHT)
                        {
                            player.dir = DIR_RIGHT;
                            playTurnSound();
                        }
                        else if (gamestate == GS_MENU)
                        {
                            playTurnSound();
                            difficulty = difficulty ? 0 : 1;
                            if (difficulty == 0) sprintf(highscore_buf, "HIGHSCORE: %d", highscore);
                            else sprintf(highscore_buf, "HIGHSCORE: %d", highscore2);
                        }
                    }break;
                    case SDLK_s:
                    case SDLK_DOWN:
                    {
                        if (gamestate == GS_GAME && player.dir != DIR_DOWN)
                        {
                            player.dir  = DIR_DOWN;
                            playTurnSound();
                        }
                        else if (gamestate == GS_MENU)
                        {
                            --enemycolor; if (enemycolor < 0) enemycolor = ENEMYCOLORS;
                            changeEnemyColor();
                        }
                    }break;
                    case SDLK_a:
                    case SDLK_LEFT:
                    {
                        if (gamestate == GS_GAME && player.dir != DIR_LEFT)
                        {
                            player.dir  = DIR_LEFT;
                            playTurnSound();
                        }
                        else if (gamestate == GS_MENU)
                        {
                            playTurnSound();
                            difficulty = difficulty ? 0 : 1;
                            if (difficulty == 0) sprintf(highscore_buf, "HIGHSCORE: %d", highscore);
                            else sprintf(highscore_buf, "HIGHSCORE: %d", highscore2);
                        }
                    }break;
                    case SDLK_RETURN:
                    {
                        Mix_PlayChannel( -1, snd_select, 0 );
                        switch(gamestate)
                        {
                            case GS_PAUSE: gamestate = GS_GAME; break;
                            case GS_MENU:
                            {
                                if (difficulty == 0) sprintf(highscore_buf, "%d", highscore);
                                else sprintf(highscore_buf, "%d", highscore2);
                                restart(GS_GAME);
                            }break;
                            case GS_DEAD: restart(GS_GAME); break;
                            case GS_GAME: gamestate = GS_PAUSE; break;
                        }
                    }break;
                    case SDLK_m:
                    {
                        muted = muted ? 0 : 1;
                        if (muted)
                        {
                            Mix_Volume(0, 0);
                            Mix_Volume(1, 0);
                            Mix_Volume(2, 0);
                            Mix_Volume(3, 0);
                        }
                        else
                        {
                            Mix_Volume(0, MAX_VOLUME);
                            Mix_Volume(1, MAX_VOLUME);
                            Mix_Volume(2, MAX_VOLUME);
                            Mix_Volume(3, MAX_VOLUME);
                        }
                    }break;
                    case SDLK_r:
                    {
                        if (gamestate == GS_MENU)
                        {
                            if (difficulty == 0)
                            {
                                highscore = 0;
                                sprintf(highscore_buf, "HIGHSCORE: %d", highscore);
                            }
                            else
                            {
                                highscore2 = 0;
                                sprintf(highscore_buf, "HIGHSCORE: %d", highscore2);
                            }
                            Mix_PlayChannel( -1, snd_explosion, 0 );
                        }
                    }break;
                    case SDLK_2: print_fps = print_fps ? 0 : 1; break;
                }
            }
        }
    }
}

void restart(int gs)
{
    gamestate = gs;

    player.rec.x = SCREEN_W / 2 - 128;
    player.rec.y = SCREEN_H / 2 + 50;
    player.rec.w = SPRITE_W;
    player.rec.h = SPRITE_W;
    player.dir   = DIR_RIGHT;
    player.speed = 4;

    star.rec.x = SCREEN_W / 2;
    star.rec.y = SCREEN_H / 2 - 50;
    star.rec.w = SPRITE_W;
    star.rec.h = SPRITE_W;

    for (int i = 0; i < ENEMY_ALLOCATIONS; ++i)
    {
        enemy[i].speed = 0;
        enemy[i].x     = -10;
        enemy[i].y     = -10;
        enemy[i].rec.x = -10;
        enemy[i].rec.y = -10;
    }

    if (score > highscore && difficulty == 0)
        highscore = score;
    else if (score > highscore2 && difficulty == 1)
        highscore2 = score;

    score = 0;
    sprintf(score_buf, "%d", score);

    num_enemies       = 0;
    spawn_enemies     = 0;
    enemy_spawn_timer = 0;
    star_ef_active    = 0;
    star_ef_timer     = 0;
    newhighscore      = 0;
    max_num_enemies = ENEMY_ALLOCATIONS;

    f = fopen("save.txt", "w");
    if (!f) printf("failed to open save.txt\n");
    else
    {
        sprintf(highscore_buf, "hs: %d\n", highscore);
        buflen = strlen(highscore_buf);
        fwrite(highscore_buf, 1, buflen, f);
        sprintf(highscore_buf, "hs2: %d\n", highscore2);
        buflen = strlen(highscore_buf);
        fwrite(highscore_buf, 1, buflen, f);
        if (difficulty == 0) sprintf(highscore_buf, "%d", highscore);
        else sprintf(highscore_buf, "%d", highscore2);
    }
    fclose(f);

    if (gamestate == GS_MENU)
    {
        if (difficulty == 0) sprintf(highscore_buf, "HIGHSCORE: %d", highscore);
        else sprintf(highscore_buf, "HIGHSCORE: %d", highscore2);
        spawn_enemies = 1;
        max_num_enemies = ENEMY_ALLOCATIONS;
    }
}

SDL_Texture *loadTexture(const char *fp)
{
    SDL_Texture *newtex;
    SDL_Surface *surface = IMG_Load(fp);
    if (!surface) printf("Image load failed! %s\n", IMG_GetError());
    else
    {
        newtex = SDL_CreateTextureFromSurface(renderer, surface);
        if (!newtex) printf("Texture creation failed! %s\n", SDL_GetError());
        SDL_FreeSurface(surface);
    }
    return newtex;
}

TTF_Font *loadFont(const char *fp, int size)
{
    TTF_Font *font = 0;
    font = TTF_OpenFont(fp, size);
    return font;
}

void renderText(SDL_Renderer *renderer, int x, int y,
    const char *text, TTF_Font *font, SDL_Color color)
{
    if (strlen(text) <= 0)
    {
        printf("Can't render empty text\n");
        return;
    }
    SDL_DestroyTexture(text_tex);
    text_tex = 0;
    SDL_Rect renderquad;
    SDL_Rect clip;
    SDL_Surface *surface = TTF_RenderText_Solid(font, text, color);
    text_tex = SDL_CreateTextureFromSurface(renderer, surface);
    clip.x = 0;
    clip.y = 0;
    clip.w = surface->w;
    clip.h = surface->h;
    renderquad.x = x;
    renderquad.y = y;
    renderquad.w = clip.w;
    renderquad.h = clip.h;
    SDL_FreeSurface(surface);
    SDL_RenderCopy(renderer, text_tex, &clip, &renderquad);
}

inline void playTurnSound()
{
    turncounter = turncounter ? 0 : 1;
    if (turncounter) Mix_PlayChannel( -1, snd_turn1, 0 );
    else Mix_PlayChannel( -1, snd_turn2, 0 );
}

inline void fpsStart()
{
    cap_start = SDL_GetTicks();
    avg_fps = counted_frames / ((SDL_GetTicks() - tick_start) / 1000.f);
    if (print_fps) sprintf(fps_buf, "FPS: %f", avg_fps);
}

inline void fpsEnd()
{
    ++counted_frames;
    tick_frame = (SDL_GetTicks() - cap_start);
    if (tick_frame < TICK_PER_FRAME)
        SDL_Delay(TICK_PER_FRAME - tick_frame);
}

inline void spawnEnemies()
{
    if (spawn_enemies && num_enemies < max_num_enemies)
    {
        enemy_spawn_timer += 3;
        if (enemy_spawn_timer > ENEMY_SPAWN_TIMER_LIMIT)
        {
            enemy[num_enemies].speed = 4;
            tmp = rand() % 4;
            switch(tmp)
            {
                // 0 = right 90 = down
                case 0://top
                {
                    enemy[num_enemies].x = rand() % SCREEN_W;
                    enemy[num_enemies].y = 16;
                    enemy[num_enemies].dir = 5 + rand() % 170;
                }break;
                case 1://left
                {
                    enemy[num_enemies].x = 16;
                    enemy[num_enemies].y = rand() % SCREEN_H;
                    enemy[num_enemies].dir = -95 + rand() % 170;
                }break;
                case 2://bottom
                {
                    enemy[num_enemies].x = rand() % SCREEN_W;
                    enemy[num_enemies].y = SCREEN_H - 16;
                    enemy[num_enemies].dir = 185 + rand() % 170;
                }break;
                case 3://right
                {
                    enemy[num_enemies].x = SCREEN_W - 16;
                    enemy[num_enemies].y = rand() % SCREEN_H;
                    enemy[num_enemies].dir = 95 + rand() % 170;
                }break;
            }
            enemy_spawn_timer = 0;
            ++num_enemies;
            if (num_enemies >= max_num_enemies) spawn_enemies = 0;
        }
    }
}

inline void changeEnemyColor()
{
    switch(enemycolor)
    {
        case 0: SDL_SetTextureColorMod(textures[2], 0xFF, 0x00, 0x00); break;
        case 1: SDL_SetTextureColorMod(textures[2], 0x00, 0xFF, 0x00); break;
        case 2: SDL_SetTextureColorMod(textures[2], 0x00, 0x00, 0xFF); break;
        case 3: SDL_SetTextureColorMod(textures[2], 0xFF, 0xFF, 0x00); break;
        case 4: SDL_SetTextureColorMod(textures[2], 0xFF, 0x00, 0xFF); break;
        case 5: SDL_SetTextureColorMod(textures[2], 0x00, 0xFF, 0xFF); break;
        case 6: SDL_SetTextureColorMod(textures[2], 0xFF, 0xFF, 0xFF); break;
        default: SDL_SetTextureColorMod(textures[2], 0xFF, 0x00, 0x00); break;
    }
}
